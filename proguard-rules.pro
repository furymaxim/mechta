# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\Work\AndroidDev\android-sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

    -optimizationpasses 5
    -dontusemixedcaseclassnames
    -dontskipnonpubliclibraryclasses
    -dontpreverify
    -verbose
    -optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

    -keep public class * extends android.app.Activity
    -keep public class * extends android.app.Application
    -keep public class * extends android.app.Service
    -keep public class * extends android.content.BroadcastReceiver
    -keep public class * extends android.content.ContentProvider
    -keep public class * extends android.app.backup.BackupAgentHelper
    -keep public class * extends android.preference.Preference
    -keep public class com.android.vending.licensing.ILicensingService

    -keepclasseswithmembernames class * {
    native <methods>;
    }

    -keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
    }

    -keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
    }

    -keepclassmembers class * extends android.app.Activity {
    public void *(android.view.View);
    }

    -keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
    }

    -keep class * implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator *;
    }

-keepattributes *Annotation*
-keepclassmembers,allowobfuscation class * {
    @com.telly.groundy.annotations.* *;
    <init>();
}
-keepnames class com.telly.groundy.generated.*
-keep class com.telly.groundy.generated.*
-keep class com.telly.groundy.ResultProxy
-keepnames class * extends com.telly.groundy.ResultProxy
-keep class * extends com.telly.groundy.GroundyTask

-keep class com.telly.groundy.*** {
public protected private *;
}
-keepattributes *Annotation*, EnclosingMethod
-keepattributes *OnSuccess*
-keepattributes *OnProgress*
-keepattributes *OnCancel*
-keepattributes *OnCallback*
-keepattributes *OnFailure*
-keepattributes *OnStart*
-keepattributes *Param*
-keepattributes *Traverse*

-libraryjars libs
-keepattributes Exceptions,InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable,*Annotation*,EnclosingMethod

-keep class su.livemechta.app.volley.** { *; }
-keep class su.livemechta.app.gcm.** { *; }

# keep all classes that might be used in XML layouts
-keep public class * extends android.view.View
-keep public class * extends android.view.ViewGroup
-keep public class * extends android.support.v4.app.Fragment
-keep public class * extends com.actionbarsherlock.app.SherlockFragment
-keep public class * extends com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity
-keep public class * extends android.support.v7.app.ActionBarActivity
-keep public class * extends android.support.v4.content.WakefulBroadcastReceiver
-keep public class * extends android.app.IntentService

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService
-dontnote com.android.vending.licensing.ILicensingService

-keep class android.support.v4.** { *; }
-keep interface android.support.v4.** { *; }
-keep enum android.support.v4.** { *; }

-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }
-keep enum android.support.v7.** { *; }

-keep class com.actionbarsherlock.** { *; }
-keep interface com.actionbarsherlock.** { *; }
-keep enum com.actionbarsherlock.** { *; }
-dontwarn com.actionbarsherlock.**

-keep class com.codeslap.** { *; }
-keep interface com.codeslap.** { *; }
-keep enum com.codeslap.** { *; }
-dontwarn com.codeslap.**

-keep class com.google.** { *; }
-keep interface com.google.** { *; }
-keep enum com.google.** { *; }
-dontwarn com.google.**

-keep class com.google.volley.** { *; }
-keep interface com.google.volley.** { *; }
-keep enum com.google.volley.** { *; }
-dontwarn com.google.volley.**

-keep class com.google.gson.** { *; }
-keep interface com.google.gson.** { *; }
-keep enum com.google.gson.** { *; }
-dontwarn com.google.gson.**

-keep class org.ocpsoft.prettytime.** { *; }
-keep interface org.ocpsoft.prettytime.** { *; }
-keep enum org.ocpsoft.prettytime.** { *; }
-dontwarn org.ocpsoft.prettytime.**

-keep class com.jeremyfeinstein.** { *; }
-keep interface com.jeremyfeinstein.** { *; }
-keep enum com.jeremyfeinstein.** { *; }
-dontwarn com.jeremyfeinstein.**

-keep class com.nineoldandroids.** { *; }
-keep interface com.nineoldandroids.** { *; }
-keep enum com.nineoldandroids.** { *; }
-dontwarn com.nineoldandroids.**

-keep class com.telly.** { *; }
-keep interface com.telly.** { *; }
-keep enum com.telly.** { *; }
-dontwarn com.telly.**

-keep class de.timroes.** { *; }
-keep interface de.timroes.** { *; }
-keep enum de.timroes.** { *; }
-dontwarn de.timroes.**

-keep class net.lingala.** { *; }
-keep interface net.lingala.** { *; }
-keep enum net.lingala.** { *; }
-dontwarn net.lingala.**

-keep class org.apache.** { *; }
-keep interface org.apache.** { *; }
-keep enum org.apache.** { *; }
-dontwarn org.apache.**


-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}






##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class su.livemechta.app.busstops.BusstopModel { *; }
-keep class su.livemechta.app.busstops.ChannelsModel { *; }
-keep class su.livemechta.app.busstops.ChannelsModel$Route { *; }
-keep class su.livemechta.app.busstops.ChannelsModel$Route$Stop { *; }
-keep class su.livemechta.app.news.ExcursionModel { *; }
-keep class su.livemechta.app.news.NewsModel { *; }
-keep class su.livemechta.app.news.OffersModel { *; }
-keep class su.livemechta.app.news.OffersModel$Offer { *; }
-keep class su.livemechta.app.schedule.ScheduleModel { *; }
-keep class su.livemechta.app.services.ServicesModel { *; }

-keep class com.google.gson.** { *; }

-keepclassmembers class * extends android.os.AsyncTask {
protected void onPreExecute();
protected *** doInBackground(...);
protected void onPostExecute(...);
}
-keep public class * implements java.lang.reflect.Type
##---------------End: proguard configuration for Gson  ----------