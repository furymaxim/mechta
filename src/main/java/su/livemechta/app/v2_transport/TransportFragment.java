package su.livemechta.app.v2_transport;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.astuetz.PagerSlidingTabStrip;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;

import io.realm.Realm;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import su.livemechta.app.Conf;
import su.livemechta.app.MainActivity;
import su.livemechta.app.R;
import su.livemechta.app.TabFragmentInterface;
import su.livemechta.app.Utils;
import su.livemechta.app.v2_rest.model.response.BusRoutesModel;
import su.livemechta.app.v2_rest.model.response.BusStopsModel;
import su.livemechta.app.v2_rest.model.response.FlightBusStopModel;
import su.livemechta.app.v2_rest.model.response.FlightsModel;
import su.livemechta.app.v2_rest.model.response.NearestBusModel;
import su.livemechta.app.v2_rest.model.response.ScheduleMinutesModel;
import su.livemechta.app.v2_rest.model.response.ScheduleModel;
import su.livemechta.app.volley.MyVolley;

/**
 * Created by beta on 12.01.17.
 */

public class TransportFragment extends Fragment {

    private Context mContext;
    private Activity mActivity;

    private StringRequest mRequestBusStops;
    private StringRequest mRequestBusRoutes;
    private String[] mTitles;
    private LayoutInflater mInflater;

    private MapView mapView;
    private MapController mMapController;
    private OverlayManager mOverlayManager;
    private MyOverlay mOverlay;
    private OverlayItem firstItem;
    private OverlayItem secondItem;
    private OverlayItem previousFirstItem;
    private OverlayItem previousSecondItem;

    private RelativeLayout layout;

    private Spinner spinner_a;
    private Spinner spinner_b;

    private List<BusStopsModel> busStopsList;
    private List<BusRoutesModel> busRoutesList;

    private List<NearestBusModel> nearestBusModelsList;

    private Response.Listener<String> busStopListener = new Response.Listener<String>() {
        @Override
        public void onResponse(String s) {
            if (s != null && !s.isEmpty()) {
                Log.v("RESPONSE_BUS_STOPS", s);
                busStopsList = new Gson().fromJson(s, BusStopsModel.Type);
            } else {
                busStopsList = getBusStopsFromCache();
                mRequestBusStops.deliverError(null);
            }

            addMarkers(busStopsList);

            final List<BusStopsModel> busStopsModels = new ArrayList<>();
            busStopsModels.add(new BusStopsModel(-1, "Выберите остановку", null, null));
            if(busStopsList!=null)
                busStopsModels.addAll(busStopsList);

            TransportSpinnerAdapter adapter = new TransportSpinnerAdapter(mInflater, busStopsModels);

            AdapterView.OnItemSelectedListener onSpinnerA = new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                    try {
                        ((TextView) parent.getChildAt(0)).setTextColor(mActivity.getResources().getColor(R.color.new_text_dark));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if(id!=-1) {
                        spinner_a_selected = true;

                        id_a = id;

                        List<OverlayItem> list = mOverlay.getOverlayItemList();
                        for (int i=0;i<list.size();i++){
                            if(list.get(i).getBalloonItem().getText().equals(busStopsModels.get(pos).getTitle())) {
                                mOverlay.setFirstSelected(list.get(i));
                                break;
                            }
                        }

                        if (spinner_b_selected) {
                            doRoutesRequest();
                        }
                    } else {
                        spinner_a_selected = false;
                        mOverlay.setFirstSelected(null);
                    }
                }

                public void onNothingSelected(AdapterView<?> parent) {

                }
            };

            AdapterView.OnItemSelectedListener onSpinnerB = new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                    try {
                        ((TextView) parent.getChildAt(0)).setTextColor(mActivity.getResources().getColor(R.color.new_text_dark));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if(id!=-1) {
                        spinner_b_selected = true;

                        id_b = id;

                        List<OverlayItem> list = mOverlay.getOverlayItemList();
                        for (int i=0;i<list.size();i++){
                            if(list.get(i).getBalloonItem().getText().equals(busStopsModels.get(pos).getTitle())) {
                                mOverlay.setSecondSelected(list.get(i));
                                break;
                            }
                        }

                        if(spinner_a_selected) {
                            doRoutesRequest();
                        }
                    } else {
                        spinner_b_selected = false;
                        mOverlay.setSecondSelected(null);
                    }
                }

                public void onNothingSelected(AdapterView<?> parent) {

                }
            };

            spinner_a.setAdapter(adapter);
            spinner_b.setAdapter(adapter);
            spinner_a.setOnItemSelectedListener(onSpinnerA);
            spinner_b.setOnItemSelectedListener(onSpinnerB);
        }
    };

    private Response.Listener<String> busRoutesListener = new Response.Listener<String>() {
        @Override
        public void onResponse(String s) {
            if (s != null && !s.isEmpty()) {
                Log.v("RESPONSE_BUS_ROUTES", s);
                busRoutesList = new Gson().fromJson(s, BusRoutesModel.Type);
            } else {
                busRoutesList = getBusRoutesFromCache();
                mRequestBusRoutes.deliverError(null);
            }
        }
    };

    private Response.ErrorListener busStopErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            Toast.makeText(mContext, mContext.getString(R.string.need_conn), Toast.LENGTH_LONG).show();
        }
    };

    private Response.ErrorListener busRoutesErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            Toast.makeText(mContext, mContext.getString(R.string.need_conn), Toast.LENGTH_LONG).show();
        }
    };

    private Fragment fragment_a = new NearestBusTabRecyclerFragment();
    private Fragment fragment_b = new ScheduleTabRecyclerFragment();

    private boolean spinner_a_selected = false;
    private boolean spinner_b_selected = false;

    private long id_a;
    private long id_b;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        mActivity = getActivity();
        setHasOptionsMenu(true);
        Realm.init(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) actionBar.setTitle(R.string.drawer_item_transport);

        mInflater = inflater;

        mTitles = getResources().getStringArray(R.array.transport_tabs);

        View view = inflater.inflate(R.layout.fragment_transport, container,false);

        mapView = (MapView) view.findViewById(R.id.map);

        layout = (RelativeLayout) view.findViewById(R.id.stops_layout);

        spinner_a = (Spinner) view.findViewById(R.id.spinner_a);
        spinner_b = (Spinner) view.findViewById(R.id.spinner_b);

        ImageView reverse = (ImageView)view.findViewById(R.id.reverse);

        reverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int temp = spinner_a.getSelectedItemPosition();
                spinner_a.setSelection(spinner_b.getSelectedItemPosition());
                spinner_b.setSelection(temp);
            }
        });

        initMap();

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Medium.ttf");
        tabs.setTypeface(tf, Typeface.BOLD);
        final ViewPager pager = (ViewPager) view.findViewById(R.id.pager);

        final TransportFragment.PagerAdapter adapter = new TransportFragment.PagerAdapter(getChildFragmentManager());
        pager.setAdapter(adapter);

        tabs.setViewPager(pager);
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                TabFragmentInterface fragment = (TabFragmentInterface) adapter.instantiateItem(pager, position);
                if (fragment != null) {
                    fragment.fragmentBecameVisible();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return view;
    }



    private ArrayList<BusStopsModel> getBusStopsFromCache() {
        String s = MyVolley.getCache(Conf.URL_BUS_STOPS);
        if (s != null && !s.isEmpty()) {
            return new Gson().fromJson(s, BusStopsModel.Type);
        }
        return new ArrayList<>();
    }

    private ArrayList<BusRoutesModel> getBusRoutesFromCache() {
        String s = MyVolley.getCache(Conf.URL_BUS_ROUTES);
        if (s != null && !s.isEmpty()) {
            return new Gson().fromJson(s, BusRoutesModel.Type);
        }
        return new ArrayList<>();
    }

    public void setMenuItems(final MenuItem menuItemCheck, final MenuItem menuItemMap) {
        menuItemMap.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                mapView.setVisibility(View.VISIBLE);
                menuItemMap.setVisible(false);
                menuItemCheck.setVisible(true);

                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)layout.getLayoutParams();
                params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

                layout.setLayoutParams(params); //causes layout update


                if(spinner_a.getSelectedItemPosition()==0) {
                    mMapController.setPositionNoAnimationTo(mOverlay.getOverlayItemList().get(0).getGeoPoint());
                    mMapController.setZoomCurrent(9);
                } else {
                    mMapController.setPositionNoAnimationTo(mOverlay.getOverlayItemList().get(spinner_a.getSelectedItemPosition()-1).getGeoPoint());
                    mMapController.setZoomCurrent(13);
                }

                return true;
            }
        });
        menuItemCheck.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                mapView.setVisibility(View.GONE);
                menuItemMap.setVisible(true);
                menuItemCheck.setVisible(false);

                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)layout.getLayoutParams();
                params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
                params.addRule(RelativeLayout.ALIGN_PARENT_TOP);

                layout.setLayoutParams(params); //causes layout update
                return true;
            }
        });
    }

    private void doRequest() {
//        Realm realm = Realm.getDefaultInstance();
//        final RealmQuery<BusStopsModel> busStops = realm.where(BusStopsModel.class);
//
//        busStops.findAll().sort("id", Sort.DESCENDING).asObservable()
//                .subscribe(new Action1<RealmResults<BusStopsModel>>() {
//                    @Override
//                    public void call(RealmResults<BusStopsModel> busStopsRestModels) {
//                        List<BusStopsModel> busStopsModels = new ArrayList<>();
//                        busStopsModels.add(new BusStopsModel(-1, "Выберите остановку", null, null));
//                        busStopsModels.addAll(busStopsRestModels);
//
//                        TransportSpinnerAdapter adapter = new TransportSpinnerAdapter(mInflater, busStopsModels);
//
//                        AdapterView.OnItemSelectedListener onSpinnerA = new AdapterView.OnItemSelectedListener() {
//                            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
//                                try {
//                                    ((TextView) parent.getChildAt(0)).setTextColor(mActivity.getResources().getColor(R.color.new_text_dark));
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//
//                                if(id!=-1) {
//                                    spinner_a_selected = true;
//
//                                    id_a = id;
//
//                                    if (spinner_b_selected) {
//                                        doRoutesRequest();
//                                    }
//                                } else {
//                                    spinner_a_selected = false;
//                                }
//                            }
//
//                            public void onNothingSelected(AdapterView<?> parent) {
//
//                            }
//                        };
//
//                        AdapterView.OnItemSelectedListener onSpinnerB = new AdapterView.OnItemSelectedListener() {
//                            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
//                                try {
//                                    ((TextView) parent.getChildAt(0)).setTextColor(mActivity.getResources().getColor(R.color.new_text_dark));
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                                if(id!=-1) {
//                                    spinner_b_selected = true;
//
//                                    id_b = id;
//
//                                    if(spinner_a_selected) {
//                                        doRoutesRequest();
//                                    }
//                                } else {
//                                    spinner_b_selected = false;
//                                }
//                            }
//
//                            public void onNothingSelected(AdapterView<?> parent) {
//
//                            }
//                        };
//
//                        spinner_a.setAdapter(adapter);
//                        spinner_b.setAdapter(adapter);
//                        spinner_a.setOnItemSelectedListener(onSpinnerA);
//                        spinner_b.setOnItemSelectedListener(onSpinnerB);
//                    }
//                });
        mRequestBusStops = new StringRequest(Request.Method.GET,
                Conf.URL_BUS_STOPS,
                busStopListener, busStopErrorListener);

        mRequestBusRoutes = new StringRequest(Request.Method.GET,
                Conf.URL_BUS_ROUTES,
                busRoutesListener, busRoutesErrorListener);

        MyVolley.getRequestQueue().add(mRequestBusStops);
        MyVolley.getRequestQueue().add(mRequestBusRoutes);
    }

    private List<NearestBusModel> generateNearestBusToShow(boolean tomorrow) {
        List<NearestBusModel> nearestBusModelsToShowTomorrow = new ArrayList<>();
        List<NearestBusModel> nearestBusModelsToShow = new ArrayList<>();

        for(int i = 0; i < busRoutesList.size(); i++) {
            BusRoutesModel busRoutesModel = busRoutesList.get(i);
            for(int j = 0; j < busRoutesModel.getFlights().size(); j++) {
                FlightsModel flightsModel = busRoutesModel.getFlights().get(j);
                boolean wasA = false;
                boolean wasB = false;
                String time_from = "";
                String time_to = "";

                for(int k = 0; k < flightsModel.getFlightBusStopModelList().size(); k++) {
                    FlightBusStopModel flightBusStopModel = flightsModel.getFlightBusStopModelList().get(k);
                    if(flightBusStopModel.getBus_stop_id()==id_a && !wasA) {
                        wasA = true;
                        time_from = flightBusStopModel.getStop_time();
                    } else if(wasA && flightBusStopModel.getBus_stop_id()==id_b) {
                        wasB = true;
                        time_to = flightBusStopModel.getStop_time();
                    }

                    if(wasA && wasB){
                        wasA = false;
                        wasB = false;

                        //TEST
                        int status = new Random().nextInt(4)-1;
                        String status_text = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.";

                        //Пригодится
                        nearestBusModelsList.add(new NearestBusModel(time_from, time_to, "", status, status_text, flightsModel.isWorking_days_availability(), flightsModel.isWeekend_days_availability()));

                        StringBuilder time_after = new StringBuilder();
                        try {
                            //Если время рейса больше текущего, то смотрим, в какие дни он ходит, если попадает в нужные дни, то считаем тупо разницу между часами текущего дня
                            //Если время рейса больше текущего, то смотрим, в какие дни он ходит, если не попадает в нужные дни, смотрим попадает ли следующий день, если да, то считаем разницу между часами сейчас и временем отправления завтра
                            //Если время рейса больше текущего, то смотрим, в какие дни он ходит, если не попадает в нужные дни, смотрим попадает ли следующий день, если нет, то не добавляем
                            //Если время рейса меньше текущего, то смотрим, в какие дни он ходит, если завтра попадает в нужные дни, то считаем разницу между сейчас и завтра
                            //Если время рейса меньше текущего, то смотрим, в какие дни он ходит, если завтра не попадает в нужные дни, то не добавляем
                            Date time = new SimpleDateFormat("HH:mm").parse(time_from);
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTimeInMillis(System.currentTimeMillis());
                            calendar.set(Calendar.HOUR_OF_DAY, time.getHours());
                            calendar.set(Calendar.MINUTE, time.getMinutes());

                            Calendar calendarNow = Calendar.getInstance();
                            calendarNow.setTimeInMillis(System.currentTimeMillis());

                            if(tomorrow){
                                calendar.add(Calendar.DATE, 1);
                            }

                            //Проверка дня
                            boolean isOkDay = false;
                            if(isWeekend(calendar.get(Calendar.DAY_OF_WEEK))) {
                                if (flightsModel.isWeekend_days_availability()) {
                                    isOkDay = true;
                                }
                            } else {
                                if (flightsModel.isWorking_days_availability()) {
                                    isOkDay = true;
                                }
                            }

                            //Го все же только на сегодня смотреть?
                            if(isOkDay) {
                                //Если часы и минуты сейчас меньше рейсовых
                                if(calendarNow.getTimeInMillis() < calendar.getTimeInMillis()) {
                                    //будет сегодня
                                    Date x = calendar.getTime();
                                    Date xy = calendarNow.getTime();
                                    long diff = x.getTime() - xy.getTime();
                                    float diffMinutes = diff / (60 * 1000);
                                    float diffHours = diffMinutes / 60;
                                    diffMinutes=diffMinutes%60;

                                    time_after.append("Через\n");
                                    if(diffHours>=1)
                                        time_after.append((int)diffHours+" ч. ");
                                    time_after.append((int)diffMinutes+" мин.");

                                    if(tomorrow)
                                        nearestBusModelsToShowTomorrow.add(new NearestBusModel(time_from, "-"+time_to, time_after.toString(), status, status_text, flightsModel.isWorking_days_availability(), flightsModel.isWeekend_days_availability()));
                                    else
                                        nearestBusModelsToShow.add(new NearestBusModel(time_from, "-"+time_to, time_after.toString(), status, status_text, flightsModel.isWorking_days_availability(), flightsModel.isWeekend_days_availability()));

                                } else {
                                    //будет завтра
                                }
                            }

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
        }

        if(tomorrow) {
            if(!nearestBusModelsToShowTomorrow.isEmpty()) {
                Collections.sort(nearestBusModelsToShowTomorrow, new Comparator<NearestBusModel>(){
                    public int compare(NearestBusModel nbm1, NearestBusModel nbm2) {
                        return nbm1.getTime_from().compareToIgnoreCase(nbm2.getTime_from());
                    }
                });
                nearestBusModelsToShowTomorrow.add(0, new NearestBusModel("Завтра", "", "", -1, "", false, false));
            } else
                nearestBusModelsToShowTomorrow.add(0, new NearestBusModel("См. расписание", "", "", -1, "", false, false));
            return nearestBusModelsToShowTomorrow;
        } else {
            if(!nearestBusModelsToShow.isEmpty()) {
                Collections.sort(nearestBusModelsToShow, new Comparator<NearestBusModel>() {
                    public int compare(NearestBusModel nbm1, NearestBusModel nbm2) {
                        return nbm1.getTime_from().compareToIgnoreCase(nbm2.getTime_from());
                    }
                });
                nearestBusModelsToShow.add(0, new NearestBusModel("Сегодня", "", "", -1, "", false, false));
            }
            return nearestBusModelsToShow;
        }

    }

    private void doRoutesRequest(){

        nearestBusModelsList = new ArrayList<>();

        List<NearestBusModel> nearestBusModelsToShow = generateNearestBusToShow(false);

        List<ScheduleModel> scheduleModels = new ArrayList<>();

        // * ставим если только в будни доступно
        for(int i=0;i<nearestBusModelsList.size();i++) {
            try {
                boolean isWeekend = nearestBusModelsList.get(i).isWeekend();
                boolean isWorkdays = nearestBusModelsList.get(i).isWorkdays();
                Date time = new SimpleDateFormat("HH:mm").parse(nearestBusModelsList.get(i).getTime_from());
                List<ScheduleMinutesModel> scheduleMinutesModels = new ArrayList<>();

                int containHour = -1;
                for(int j=0;j<scheduleModels.size();j++) {
                    if(scheduleModels.get(j).getTime_hour().equals(String.format("%02d", time.getHours()))) {
                        containHour = j;
                    }
                }

                int status = nearestBusModelsList.get(i).getStatus();
                String status_text = nearestBusModelsList.get(i).getStatus_text();

                if(containHour!=-1) {
                    //Добавляем минуты в scheduleModel.get(j)
                    scheduleMinutesModels = scheduleModels.get(containHour).getTime_minutes();
                    String minute = String.format("%02d", time.getMinutes());
                    if(!isWeekend)
                        minute+="*";
                    scheduleMinutesModels.add(new ScheduleMinutesModel(minute, status, status_text));
                    scheduleModels.get(containHour).setTime_minutes(scheduleMinutesModels);
                } else {
                    //Добавляем новую модель
                    String minute = String.format("%02d", time.getMinutes());
                    if(!isWeekend)
                        minute+="*";
                    scheduleMinutesModels.add(new ScheduleMinutesModel(minute, status, status_text));
                    scheduleModels.add(new ScheduleModel(String.format("%02d", time.getHours()), scheduleMinutesModels));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Collections.sort(scheduleModels, new Comparator<ScheduleModel>(){
            public int compare(ScheduleModel sm1, ScheduleModel sm2) {
                return sm1.getTime_hour().compareToIgnoreCase(sm2.getTime_hour());
            }
        });

        for (ScheduleModel scheduleModel:scheduleModels) {
            Collections.sort(scheduleModel.getTime_minutes(), new Comparator<ScheduleMinutesModel>(){
                public int compare(ScheduleMinutesModel smm1, ScheduleMinutesModel smm2) {
                    return smm1.getTime_minute().compareToIgnoreCase(smm2.getTime_minute());
                }
            });
        }

        List<NearestBusModel> nearestBusModelsToShowTomorrow = generateNearestBusToShow(true);

        nearestBusModelsToShow.addAll(nearestBusModelsToShowTomorrow);

        if(nearestBusModelsToShow.isEmpty()) {
            nearestBusModelsToShow.add(new NearestBusModel("См. расписание", "", "", -1, "", false, false));
        }

        if(scheduleModels.size()!=0)
            scheduleModels.add(0, new ScheduleModel());

        ((NearestBusTabRecyclerFragment)fragment_a).showNearestBus(nearestBusModelsToShow);
        ((ScheduleTabRecyclerFragment)fragment_b).showSchedule(scheduleModels);
    }

    private boolean isWeekend(int day) {
        if(day==7 || day==1)
            return true;
        else
            return false;
    }

    private void initMap() {
//        WebSettings webSettings = webView.getSettings();
//        webSettings.setJavaScriptEnabled(true);
//        webSettings.setDomStorageEnabled(true);
//
//        try {
//            InputStream is = mContext.getAssets().open("index.html");
//            byte[] buffer = new byte[is.available()];
//            if (is.read(buffer) <= 0)
//                Log.e("Log", "Can't read embedded assets index.html file!");
//            is.close();
//
//            String htmlText = new String(buffer);
//            webView.loadDataWithBaseURL("http://ru.yandex.api.yandexmapswebviewexample.ymapapp",
//                    htmlText, "text/html", "UTF-8", null);
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        mapView.showZoomButtons(true);
        mapView.showFindMeButton(false);
        mapView.showJamsButton(false);

        mMapController = mapView.getMapController();

        mOverlayManager = mMapController.getOverlayManager();

        mOverlay = new MyOverlay(mMapController);

        mOverlayManager.addOverlay(mOverlay);

        // Determining the user's location
//        mOverlayManager.getMyLocation().setEnabled(true);
//        mOverlayManager.getMyLocation().findMe();

    }

    @Override
    public void onResume() {
        super.onResume();
        doRequest();
        Utils.sendGATracker(mActivity, "Transport");
    }

    private void addMarkers(List<BusStopsModel> list) {

        List<OverlayItem> overlayItemList = new ArrayList<>();

        double lat = 0;
        double lng = 0;

        for(BusStopsModel busStopsModel:list) {
            lat = Double.parseDouble(busStopsModel.getLatitude());
            lng = Double.parseDouble(busStopsModel.getLongitude());

            OverlayItem overlayItem = new OverlayItem(new GeoPoint(lat, lng), mContext.getResources().getDrawable(R.drawable.ic_comma));
            BalloonItem balloonItem = new BalloonItem(mContext, new GeoPoint(lat, lng));
            balloonItem.setText(busStopsModel.getTitle());
            overlayItem.setBalloonItem(balloonItem);

            mOverlay.addOverlayItem(overlayItem);
            overlayItemList.add(overlayItem);
        }

        mOverlay.setItems(overlayItemList);

        mMapController.notifyRepaint();

        mMapController.setPositionAnimationTo(new GeoPoint(lat, lng));
    }


    public class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public int getCount() {
            return mTitles.length;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: return fragment_a;
                default: return fragment_b;
            }
        }
    }

    private class MyOverlay extends Overlay {

        List<OverlayItem> items;

        public MyOverlay(MapController mapController) {
            super(mapController);
        }

        public void setItems(List<OverlayItem> items){
            this.items = items;
        }

        @Override
        public boolean onSingleTapUp(float var1, float var2) {
            OverlayItem var3;
            if((var3 = this.a(var1, var2)) == null) {
                return false;
            } else {
                if(var3.getBalloonItem() != null) {
                    int position = 0;
                    for (int i=0;i<busStopsList.size();i++) {
                        if(busStopsList.get(i).getTitle().equals(var3.getBalloonItem().getText())) {
                            position = i+1;
                            break;
                        }
                    }
                    if(spinner_a.getSelectedItemPosition()==0) {
                        spinner_a.setSelection(position);
                    } else {
                        spinner_b.setSelection(position);
                    }
                }

                return true;
            }
        }

        public List<OverlayItem> getOverlayItemList() {
            return items;
        }

        public void setFirstSelected(OverlayItem var3) {
            firstItem = var3;
            if(firstItem!=null)
                firstItem.setDrawable(mContext.getResources().getDrawable(R.drawable.selected_mark));
            if(previousFirstItem!=null) {
                if(previousSecondItem==null)
                    previousFirstItem.setDrawable(mContext.getResources().getDrawable(R.drawable.default_mark));
                else if (!previousFirstItem.equals(previousSecondItem))
                    previousFirstItem.setDrawable(mContext.getResources().getDrawable(R.drawable.default_mark));
            }
            previousFirstItem = var3;

            mMapController.notifyRepaint();
        }

        public void setSecondSelected(OverlayItem var3) {
            secondItem = var3;
            if(secondItem!=null)
                secondItem.setDrawable(mContext.getResources().getDrawable(R.drawable.selected_mark));
            if(previousSecondItem!=null) {
                if(previousFirstItem==null)
                    previousSecondItem.setDrawable(mContext.getResources().getDrawable(R.drawable.default_mark));
                else if(!previousSecondItem.equals(previousFirstItem))
                    previousSecondItem.setDrawable(mContext.getResources().getDrawable(R.drawable.default_mark));
            }
            previousSecondItem = var3;

            mMapController.notifyRepaint();
        }
    }
}
