package su.livemechta.app.v2_transport;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import io.realm.RealmResults;
import su.livemechta.app.R;
import su.livemechta.app.v2_rest.model.response.BusStopsModel;

/**
 * Created by beta on 15.01.17.
 */

public class TransportSpinnerAdapter extends BaseAdapter implements android.widget.SpinnerAdapter {
    private LayoutInflater mInflater;
    private List<BusStopsModel> mItems;

    public TransportSpinnerAdapter(LayoutInflater inflater, List<BusStopsModel> items) {
        mInflater = inflater;
        mItems = items;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public BusStopsModel getItem(int i) {
        if (i >= mItems.size() || i < 0) {
            return new BusStopsModel();
        }
        return mItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return mItems.get(i).getId();
    }

    public void setItems(RealmResults<BusStopsModel> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = mInflater.inflate(R.layout.spinner_item2, null);
        ((TextView) view.findViewById(android.R.id.text1)).setText(mItems.get(i).getTitle());
        return view;
    }

    @Override
    public View getDropDownView(int i, View view, ViewGroup parent) {
        view = mInflater.inflate(R.layout.spinner_item, null);
        ((TextView) view.findViewById(android.R.id.text1)).setText(mItems.get(i).getTitle());
        return view;
    }
}