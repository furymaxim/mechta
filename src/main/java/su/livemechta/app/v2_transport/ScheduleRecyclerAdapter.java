package su.livemechta.app.v2_transport;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import it.sephiroth.android.library.tooltip.Tooltip;
import su.livemechta.app.R;
import su.livemechta.app.common.RecyclerViewClickListener;
import su.livemechta.app.v2_rest.model.response.ScheduleMinutesModel;
import su.livemechta.app.v2_rest.model.response.ScheduleModel;

/**
 * Created by beta on 05.01.17.
 */

public class ScheduleRecyclerAdapter extends RecyclerView.Adapter<ScheduleRecyclerAdapter.ScheduleViewHolder> {

    private final int TYPE_STAR_DESCRIPTION = 0;
    private final int TYPE_ITEM = 1;

    private List<ScheduleModel> routes;
    private Context mContext;

    public ScheduleRecyclerAdapter(List<ScheduleModel> routes, Context context) {
        this.routes = routes;
        this.mContext = context;
    }

    /**
     * Создание новых View и ViewHolder элемента списка, которые впоследствии могут переиспользоваться.
     */
    @Override
    public ScheduleViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        if(i==TYPE_ITEM) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_item_schedule, viewGroup, false);
            return new ScheduleViewHolder(v);
        } else {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_item_star_description, viewGroup, false);
            return new ScheduleViewHolder(v);
        }
    }

    /**
     * Заполнение виджетов View данными из элемента списка с номером i
     */
    @Override
    public void onBindViewHolder(ScheduleViewHolder viewHolder, int i) {
        if(i!=0)
            viewHolder.setItem(routes.get(i));
    }

    @Override
    public int getItemViewType(int position) {
        if(position==0)
            return TYPE_STAR_DESCRIPTION;
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return routes.size();
    }

    public class ScheduleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ScheduleModel nItem;

        private TextView time_hour;
        private LinearLayout minutes_layout;

        public ScheduleViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            time_hour = (TextView) itemView.findViewById(R.id.time_hour);
            minutes_layout = (LinearLayout) itemView.findViewById(R.id.minutes_layout);
        }

        public void setItem(ScheduleModel item) {
            nItem = item;
            time_hour.setText(nItem.getTime_hour());

            minutes_layout.removeAllViews();

            for (ScheduleMinutesModel scheduleMinutes: nItem.getTime_minutes()) {
                View v = LayoutInflater.from(mContext).inflate(R.layout.recycler_item_minute_schedule, null, false);
                TextView time_minute = (TextView) v.findViewById(R.id.time_minute);

                time_minute.setText(scheduleMinutes.getTime_minute());

                final Tooltip.TooltipView tooltipView;

                switch (scheduleMinutes.getStatus()) {
                    case 0: {
                        time_minute.setBackgroundResource(R.drawable.circle_border_green);
                        tooltipView = Tooltip.make(mContext,
                                new Tooltip.Builder(101)
                                        .anchor(time_minute, Tooltip.Gravity.TOP)
                                        .closePolicy(new Tooltip.ClosePolicy()
                                                .insidePolicy(true, false)
                                                .outsidePolicy(true, false), 3000)
                                        .activateDelay(250)
                                        .withStyleId(R.style.MyGreenTooltip)
                                        .showDelay(100)
                                        .text(scheduleMinutes.getStatus_text())
                                        .maxWidth(1000)
                                        .withArrow(true)
                                        .build()
                        );
                        break;
                    }
                    case 1: {
                        time_minute.setBackgroundResource(R.drawable.circle_border_yellow);
                        tooltipView = Tooltip.make(mContext,
                                new Tooltip.Builder(102)
                                        .anchor(time_minute, Tooltip.Gravity.TOP)
                                        .closePolicy(new Tooltip.ClosePolicy()
                                                .insidePolicy(true, false)
                                                .outsidePolicy(true, false), 3000)
                                        .activateDelay(250)
                                        .withStyleId(R.style.MyYellowTooltip)
                                        .showDelay(100)
                                        .text(scheduleMinutes.getStatus_text())
                                        .maxWidth(1000)
                                        .withArrow(true)
                                        .build()
                        );
                        break;
                    }
                    case 2: {
                        time_minute.setBackgroundResource(R.drawable.circle_border_red);
                        tooltipView = Tooltip.make(mContext,
                                new Tooltip.Builder(103)
                                        .anchor(time_minute, Tooltip.Gravity.TOP)
                                        .closePolicy(new Tooltip.ClosePolicy()
                                                .insidePolicy(true, false)
                                                .outsidePolicy(true, false), 3000)
                                        .activateDelay(250)
                                        .withStyleId(R.style.MyRedTooltip)
                                        .showDelay(100)
                                        .text(scheduleMinutes.getStatus_text())
                                        .maxWidth(1000)
                                        .withArrow(true)
                                        .build()
                        );
                        break;
                    }
                    default: {
                        time_minute.setBackgroundDrawable(null);
                        tooltipView = null;
                        break;
                    }
                }

                time_minute.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        tooltipView.show();
                    }
                });

                minutes_layout.addView(v);
            }



        }

        @Override
        public void onClick(View view) {

        }
    }

}
