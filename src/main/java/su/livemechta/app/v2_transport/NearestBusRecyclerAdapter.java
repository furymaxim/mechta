package su.livemechta.app.v2_transport;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import it.sephiroth.android.library.tooltip.Tooltip;
import su.livemechta.app.R;
import su.livemechta.app.v2_rest.model.response.NearestBusModel;

/**
 * Created by beta on 05.01.17.
 */

public class NearestBusRecyclerAdapter extends RecyclerView.Adapter<NearestBusRecyclerAdapter.NearestBusViewHolder> {

    private List<NearestBusModel> routes;
    private Context mContext;

    public NearestBusRecyclerAdapter(List<NearestBusModel> routes, Context context) {
        this.routes = routes;
        this.mContext = context;
    }

    /**
     * Создание новых View и ViewHolder элемента списка, которые впоследствии могут переиспользоваться.
     */
    @Override
    public NearestBusViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_item_nearest_bus, viewGroup, false);
        return new NearestBusViewHolder(v);
    }

    /**
     * Заполнение виджетов View данными из элемента списка с номером i
     */
    @Override
    public void onBindViewHolder(NearestBusViewHolder viewHolder, int i) {
        if(!routes.get(i).getTime_to().isEmpty()) {
            if (i == 0)
                viewHolder.setItem(routes.get(i), mContext.getResources().getColor(R.color.new_green));
            else if (i == 1)
                viewHolder.setItem(routes.get(i), mContext.getResources().getColor(R.color.new_text_dark));
            else
                viewHolder.setItem(routes.get(i), mContext.getResources().getColor(R.color.new_text_light_darker));
        } else {
            viewHolder.setItem(routes.get(i), mContext.getResources().getColor(R.color.new_blue));
        }
    }

    @Override
    public int getItemCount() {
        return routes.size();
    }

    public class NearestBusViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private NearestBusModel nItem;

        private TextView time_from;
        private TextView time_to;
        private TextView time_after;
        private ImageView badge;

        private Tooltip.TooltipView tooltipView;

        public NearestBusViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            time_from = (TextView) itemView.findViewById(R.id.time_from);
            time_to = (TextView) itemView.findViewById(R.id.time_to);
            time_after = (TextView) itemView.findViewById(R.id.time_after);
            badge = (ImageView) itemView.findViewById(R.id.image_badge);
        }

        public void setItem(NearestBusModel item, int color) {
            nItem = item;
            time_from.setText(nItem.getTime_from());
            time_to.setText(nItem.getTime_to());
            time_after.setText(nItem.getTime_after());
            if(color==mContext.getResources().getColor(R.color.new_blue))
                time_from.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
            else
                time_from.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
            time_after.setTextColor(color);

            //TODO есть ли пуши по данному отправлению?!
            switch (nItem.getStatus()) {
                case 0: {
                    badge.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_green));
                    tooltipView = Tooltip.make(mContext,
                            new Tooltip.Builder(104)
                                    .anchor(badge, Tooltip.Gravity.TOP)
                                    .closePolicy(new Tooltip.ClosePolicy()
                                            .insidePolicy(true, false)
                                            .outsidePolicy(true, false), 3000)
                                    .activateDelay(250)
                                    .withStyleId(R.style.MyGreenTooltip)
                                    .showDelay(100)
                                    .text(nItem.getStatus_text())
                                    .maxWidth(1000)
                                    .withArrow(true)
                                    .build()
                    );
                    break;
                }
                case 1: {
                    badge.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_yellow));
                    tooltipView = Tooltip.make(mContext,
                            new Tooltip.Builder(105)
                                    .anchor(badge, Tooltip.Gravity.TOP)
                                    .closePolicy(new Tooltip.ClosePolicy()
                                            .insidePolicy(true, false)
                                            .outsidePolicy(true, false), 3000)
                                    .activateDelay(250)
                                    .withStyleId(R.style.MyYellowTooltip)
                                    .showDelay(100)
                                    .text(nItem.getStatus_text())
                                    .maxWidth(1000)
                                    .withArrow(true)
                                    .build()
                    );
                    break;
                }
                case 2: {
                    badge.setImageDrawable(mContext.getResources().getDrawable(R.drawable.circle_red));
                    tooltipView = Tooltip.make(mContext,
                            new Tooltip.Builder(106)
                                    .anchor(badge, Tooltip.Gravity.TOP)
                                    .closePolicy(new Tooltip.ClosePolicy()
                                            .insidePolicy(true, false)
                                            .outsidePolicy(true, false), 3000)
                                    .activateDelay(250)
                                    .withStyleId(R.style.MyRedTooltip)
                                    .showDelay(100)
                                    .text(nItem.getStatus_text())
                                    .maxWidth(1000)
                                    .withArrow(true)
                                    .build()
                    );
                    break;
                }
                default: {
                    badge.setImageDrawable(null);
                    tooltipView = null;
                    break;
                }
            }
        }

        @Override
        public void onClick(View view) {
            if(tooltipView!=null)
                tooltipView.show();
        }
    }

}
