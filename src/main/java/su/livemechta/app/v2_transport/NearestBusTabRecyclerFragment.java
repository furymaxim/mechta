package su.livemechta.app.v2_transport;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import rx.functions.Action1;
import su.livemechta.app.Conf;
import su.livemechta.app.R;
import su.livemechta.app.TabFragmentInterface;
import su.livemechta.app.Utils;
import su.livemechta.app.v2_news.NewsRecyclerAdapter;
import su.livemechta.app.v2_rest.model.response.BusRoutesModel;
import su.livemechta.app.v2_rest.model.response.BusStopsModel;
import su.livemechta.app.v2_rest.model.response.FlightBusStopModel;
import su.livemechta.app.v2_rest.model.response.NearestBusModel;
import su.livemechta.app.v2_rest.model.response.NewsRestModel;
import su.livemechta.app.volley.MyVolley;

/**
 * Created by beta on 05.01.17.
 */

public class NearestBusTabRecyclerFragment extends Fragment implements TabFragmentInterface {

    private Context mContext;
    private Activity mActivity;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        mActivity = getActivity();
        Realm.init(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recyclerview, null);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);

        return view;
    }

    public void showNearestBus(List<NearestBusModel> nearestBusModelList) {

        mAdapter = new NearestBusRecyclerAdapter(nearestBusModelList, mContext);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void fragmentBecameVisible() {

    }
}




