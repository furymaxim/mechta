package su.livemechta.app.fcm;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.util.HashMap;
import java.util.Map;

import su.livemechta.app.Conf;
import su.livemechta.app.volley.MyVolley;


public class MyFireBaseInstanceIdService extends FirebaseInstanceIdService {

    private String token;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        sendRegistrationIdToBackend();
    }

    private void sendRegistrationIdToBackend() {
        token = FirebaseInstanceId.getInstance().getToken();
        if (token == null) return;

        StringRequest myReq = new StringRequest(Request.Method.POST,
                Conf.URL_FCM_SERVER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {}
                }, null
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", token);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };

        MyVolley.getRequestQueue().add(myReq);
    }
}
