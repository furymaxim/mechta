package su.livemechta.app.common;

import android.view.View;

/**
 * Created by beta on 05.01.17.
 */

public interface RecyclerViewClickListener {
    void onRowClicked(int position);
    void onViewClicked(View v, int position);
}
