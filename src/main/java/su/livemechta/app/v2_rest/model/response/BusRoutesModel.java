package su.livemechta.app.v2_rest.model.response;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import io.realm.annotations.PrimaryKey;

/**
 * Created by admin on 25.02.2017.
 */
public class BusRoutesModel {

    public static java.lang.reflect.Type Type = new TypeToken<List<BusRoutesModel>>(){}.getType();

    @SerializedName("id")
    @PrimaryKey
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("flights")
    private List<FlightsModel> flights;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FlightsModel> getFlights() {
        return flights;
    }

    public void setFlights(List<FlightsModel> flights) {
        this.flights = flights;
    }
}
