package su.livemechta.app.v2_rest.model.response;

import java.util.List;

/**
 * Created by admin on 25.02.2017.
 */
public class ScheduleMinutesModel {

    private String time_minute;

    private int status;

    private String status_text;

    public ScheduleMinutesModel() {
    }

    public ScheduleMinutesModel(String time_minute, int status, String status_text) {
        this.time_minute = time_minute;
        this.status = status;
        this.status_text = status_text;
    }

    public String getTime_minute() {
        return time_minute;
    }

    public void setTime_minute(String time_minute) {
        this.time_minute = time_minute;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatus_text() {
        return status_text;
    }

    public void setStatus_text(String status_text) {
        this.status_text = status_text;
    }
}
