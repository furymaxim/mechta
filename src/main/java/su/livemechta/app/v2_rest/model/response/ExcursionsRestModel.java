package su.livemechta.app.v2_rest.model.response;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by beta on 06.01.17.
 */

public class ExcursionsRestModel extends RealmObject {
    public static java.lang.reflect.Type Type = new TypeToken<List<ExcursionsRestModel>>(){}.getType();

    @SerializedName("id")
    @PrimaryKey
    private int id;

    @SerializedName("start_date")
    private String start_date;

    @SerializedName("title")
    private String title;

    @SerializedName("detail_url")
    private String detail_url;

    @SerializedName("description")
    private String description;

    @SerializedName("published_at")
    private String published_at;

    @SerializedName("photo")
    private String photo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail_url() {
        return detail_url;
    }

    public void setDetail_url(String detail_url) {
        this.detail_url = detail_url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublished_at() {
        return published_at;
    }

    public void setPublished_at(String published_at) {
        this.published_at = published_at;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}