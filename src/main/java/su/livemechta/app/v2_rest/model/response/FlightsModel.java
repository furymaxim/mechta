package su.livemechta.app.v2_rest.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 25.02.2017.
 */
public class FlightsModel {
    @SerializedName("id")
    private int id;

    @SerializedName("express")
    private boolean express;

    @SerializedName("weekend_days_availability")
    private boolean weekend_days_availability;

    @SerializedName("working_days_availability")
    private boolean working_days_availability;

    @SerializedName("bus_stops")
    private List<FlightBusStopModel> flightBusStopModelList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isExpress() {
        return express;
    }

    public void setExpress(boolean express) {
        this.express = express;
    }

    public boolean isWeekend_days_availability() {
        return weekend_days_availability;
    }

    public void setWeekend_days_availability(boolean weekend_days_availability) {
        this.weekend_days_availability = weekend_days_availability;
    }

    public boolean isWorking_days_availability() {
        return working_days_availability;
    }

    public void setWorking_days_availability(boolean working_days_availability) {
        this.working_days_availability = working_days_availability;
    }

    public List<FlightBusStopModel> getFlightBusStopModelList() {
        return flightBusStopModelList;
    }

    public void setFlightBusStopModelList(List<FlightBusStopModel> flightBusStopModelList) {
        this.flightBusStopModelList = flightBusStopModelList;
    }
}
