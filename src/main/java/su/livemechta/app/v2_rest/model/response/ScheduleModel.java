package su.livemechta.app.v2_rest.model.response;

import java.util.List;

/**
 * Created by admin on 25.02.2017.
 */
public class ScheduleModel {

    private String time_hour;

    private List<ScheduleMinutesModel> time_minutes;

    public ScheduleModel() {
    }

    public ScheduleModel(String time_hour, List<ScheduleMinutesModel> time_minutes) {
        this.time_hour = time_hour;
        this.time_minutes = time_minutes;
    }

    public String getTime_hour() {
        return time_hour;
    }

    public void setTime_hour(String time_hour) {
        this.time_hour = time_hour;
    }

    public List<ScheduleMinutesModel> getTime_minutes() {
        return time_minutes;
    }

    public void setTime_minutes(List<ScheduleMinutesModel> time_minutes) {
        this.time_minutes = time_minutes;
    }
}
