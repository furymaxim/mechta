package su.livemechta.app.v2_rest.model.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 25.02.2017.
 */
public class FlightBusStopModel {
    @SerializedName("bus_stop_id")
    private int bus_stop_id;

    @SerializedName("stop_time")
    private String stop_time;

    public int getBus_stop_id() {
        return bus_stop_id;
    }

    public void setBus_stop_id(int bus_stop_id) {
        this.bus_stop_id = bus_stop_id;
    }

    public String getStop_time() {
        return stop_time;
    }

    public void setStop_time(String stop_time) {
        this.stop_time = stop_time;
    }
}
