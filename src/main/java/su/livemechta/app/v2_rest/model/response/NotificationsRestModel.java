package su.livemechta.app.v2_rest.model.response;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.Date;
import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by beta on 17.01.17.
 */

public class NotificationsRestModel extends RealmObject {
    public static String DateFormat = "yyyy-MM-dd HH:mm:ss";
    public static java.lang.reflect.Type Type = new TypeToken<List<NotificationsRestModel>>(){}.getType();

    @SerializedName("id")
    @PrimaryKey
    private int id;

    @SerializedName("published_at")
    private Date published_at;

    @SerializedName("push_type")
    private String push_type;

    @SerializedName("body")
    private String body;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPublished_at() {
        return published_at;
    }

    public void setPublished_at(Date published_at) {
        this.published_at = published_at;
    }

    public String getPush_type() {
        return push_type;
    }

    public void setPush_type(String push_type) {
        this.push_type = push_type;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
