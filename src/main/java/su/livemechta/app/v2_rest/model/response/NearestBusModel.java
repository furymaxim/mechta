package su.livemechta.app.v2_rest.model.response;

/**
 * Created by admin on 25.02.2017.
 */
public class NearestBusModel {

    private String time_from;

    private String time_to;

    private String time_after;

    private int status;

    private String status_text;

    private boolean isWeekend;

    private boolean isWorkdays;

    public NearestBusModel() {
    }

    public NearestBusModel(String time_from, String time_to, String time_after, int status, String status_text, boolean isWorkdays, boolean isWeekend) {
        this.time_from = time_from;
        this.time_to = time_to;
        this.time_after = time_after;
        this.status = status;
        this.status_text = status_text;
        this.isWeekend = isWeekend;
        this.isWorkdays = isWorkdays;
    }

    public String getTime_from() {
        return time_from;
    }

    public void setTime_from(String time_from) {
        this.time_from = time_from;
    }

    public String getTime_to() {
        return time_to;
    }

    public void setTime_to(String time_to) {
        this.time_to = time_to;
    }

    public String getTime_after() {
        return time_after;
    }

    public void setTime_after(String time_after) {
        this.time_after = time_after;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatus_text() {
        return status_text;
    }

    public void setStatus_text(String status_text) {
        this.status_text = status_text;
    }

    public boolean isWeekend() {
        return isWeekend;
    }

    public void setWeekend(boolean weekend) {
        isWeekend = weekend;
    }

    public boolean isWorkdays() {
        return isWorkdays;
    }

    public void setWorkdays(boolean workdays) {
        isWorkdays = workdays;
    }
}
