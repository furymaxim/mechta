package su.livemechta.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import su.livemechta.app.v2_callback.CallbackFragment;
import su.livemechta.app.v2_news.NewsFragment;
import su.livemechta.app.v2_notifications.NotificationsFragment;
import su.livemechta.app.v2_services.ServicesFragment;
import su.livemechta.app.v2_settings.SettingsFragment;
import su.livemechta.app.v2_transport.TransportFragment;
import su.livemechta.app.volley.NoFragment;

public class MainActivity extends AppCompatActivity {

    private Context mContext;
    private FragmentManager fm;

    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String SENDER_ID = "944957273525";

    private Fragment newsFragment = new NewsFragment();
    private Fragment servicesFragment = new ServicesFragment();
    private Fragment callbackFragment = new CallbackFragment();
    private Fragment transportFragment = new TransportFragment();
    private Fragment notificationsFragment = new NotificationsFragment();
    private Fragment settingsFragment = new SettingsFragment();
    private Fragment noFragment = new NoFragment();

    private final String FRAGMENT_NUM_KEY = "MainActivity:FragmentNum";
    private int mFragmentNum = 1;

    private Drawer drawerResult = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = getApplicationContext();
        fm = getSupportFragmentManager();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawerResult = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withSelectedItemByPosition(1)
                .withHeader(R.layout.drawer_header)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_item_news).withIcon(R.drawable.ic_alarm_new),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_transport).withIcon(R.drawable.ic_bus_new),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_services).withIcon(R.drawable.ic_bag_new),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_callback).withIcon(R.drawable.ic_phoe_new),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_notifications).withIcon(R.drawable.ic_bell_new),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_feedback).withIcon(R.drawable.ic_star_new),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_settings).withIcon(R.drawable.ic_settings_new)
                )
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
                        InputMethodManager inputMethodManager = (InputMethodManager) MainActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(MainActivity.this.getCurrentFocus().getWindowToken(), 0);
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {

                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {

                    }
                })
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        setFragment(position);
                        return false;
                    }
                })
                .build();

        if (savedInstanceState != null && savedInstanceState.containsKey(FRAGMENT_NUM_KEY)) {
            mFragmentNum = savedInstanceState.getInt(FRAGMENT_NUM_KEY, 0);
        }

        drawerResult.setSelectionAtPosition(mFragmentNum);


        try {
            Log.i("FCMToken", "============================================================");
            Log.i("FCMToken", FirebaseInstanceId.getInstance().getToken());
            Log.i("FCMToken", "============================================================");
        } catch (Exception e) {

        }

    }

    private void setFragment(int position) {
        Fragment fr;
        String tag;

        switch (position) {
            case 1:
                fr = newsFragment;
                tag = "news";
                break;
            case 2:
                fr = transportFragment;
                tag = "transport";
                break;
            case 3:
                fr = servicesFragment;
                tag = "services";
                break;
            case 4:
                fr = callbackFragment;
                tag = "callback";
                break;
            case 5:
                fr = notificationsFragment;
                tag = "notifications";
                break;
            case 6:
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=su.livemechta.app"));
                    startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(this, getString(R.string.google_play_not_found), Toast.LENGTH_SHORT).show();
                }
                return;
            case 7:
                fr = settingsFragment;
                tag = "settings";
                break;
            default:
                fr = noFragment;
                tag = "no";
        }

        Fragment backFragment = fm.findFragmentByTag(tag);
        if (backFragment == null || !backFragment.isVisible()) {
            fm.beginTransaction()
                    .addToBackStack(String.valueOf(position))
                    .replace(R.id.content_frame, fr, tag)
                    .commit();
        }
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(this, getString(R.string.device_not_supported), Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerResult.isDrawerOpen()) {
            drawerResult.closeDrawer();
        }
        if (fm.getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
            if (fm.getBackStackEntryAt(0) != null) {
                FragmentManager.BackStackEntry lastEntry = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1);
                setFragment(Integer.parseInt(lastEntry.getName()));
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // mDrawerToggle.syncState();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(FRAGMENT_NUM_KEY, mFragmentNum);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);

        Fragment myFragment = fm.findFragmentByTag("transport");
        if (myFragment != null && myFragment.isVisible()) {
            menu.findItem(R.id.buttonMap).setVisible(true);
            menu.findItem(R.id.buttonCheck).setVisible(false);
            ((TransportFragment) myFragment).setMenuItems(menu.findItem(R.id.buttonCheck), menu.findItem(R.id.buttonMap));
        } else {
            menu.findItem(R.id.buttonMap).setVisible(false);
            menu.findItem(R.id.buttonCheck).setVisible(false);
            menu.findItem(R.id.buttonMap).setOnMenuItemClickListener(null);
            menu.findItem(R.id.buttonCheck).setOnMenuItemClickListener(null);
        }
//        if (getCurrentFragment() != null) {
//            if (getCurrentFragment().getTag().equals("transport")) {
//                menu.findItem(R.id.buttonMap).setVisible(true);
//                menu.findItem(R.id.buttonCheck).setVisible(false);
//            } else {
//                menu.findItem(R.id.buttonMap).setVisible(false);
//                menu.findItem(R.id.buttonCheck).setVisible(false);
//            }
//        } else {
//            menu.findItem(R.id.buttonCheck).setVisible(false);
//            menu.findItem(R.id.buttonCheck).setVisible(false);
//        }
        return true;
    }

    private Fragment getCurrentFragment() {
        int stackCount = fm.getBackStackEntryCount();

        if (fm.getFragments() != null) {
            try {
                return fm.getFragments().get(stackCount > 0 ? stackCount - 1 : stackCount);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else
            return null;

    }
}
