package su.livemechta.app.v2_news;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import rx.functions.Action1;
import su.livemechta.app.Conf;
import su.livemechta.app.R;
import su.livemechta.app.TabFragmentInterface;
import su.livemechta.app.Utils;
import su.livemechta.app.v2_rest.model.response.NewsRestModel;
import su.livemechta.app.v2_rest.model.response.OffersRestModel;
import su.livemechta.app.volley.MyVolley;

/**
 * Created by beta on 06.01.17.
 */

public class OffersTabRecyclerFragment extends Fragment implements Response.ErrorListener, Response.Listener<String>, TabFragmentInterface {

    private Context mContext;
    private Activity mActivity;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    private StringRequest mRequest;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        mActivity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recyclerview, null);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);

        return view;
    }

    private void doRequest() {
        Realm realm = Realm.getDefaultInstance();
        final RealmQuery<OffersRestModel> offers = realm.where(OffersRestModel.class);

        offers.findAll().sort("id", Sort.DESCENDING).asObservable()
                .subscribe(new Action1<RealmResults<OffersRestModel>>() {
                    @Override
                    public void call(RealmResults<OffersRestModel> offersRestModels) {
                        mAdapter = new OffersRecyclerAdapter(offersRestModels, mContext);
                        mRecyclerView.setAdapter(mAdapter);
                    }
                });

        mRequest = new StringRequest(Request.Method.GET,
                Conf.URL_OFFERS,
                this, this);

        MyVolley.getRequestQueue().add(mRequest);
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        Toast.makeText(mContext, mContext.getString(R.string.need_conn), Toast.LENGTH_LONG).show();
        /*String s = MyVolley.getCache(Conf.URL_OFFERS);
        if (s != null && !s.isEmpty()) {
            offersList = new Gson().fromJson(s, OffersRestModel.Type);
            mAdapter = new OffersRecyclerAdapter(offersList, mContext);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            Toast.makeText(mContext, mContext.getString(R.string.need_conn), Toast.LENGTH_LONG).show();
        }*/

    }

    @Override
    public void onResponse(String s) {
        if (s != null && !s.isEmpty()) {
            final List<OffersRestModel> offersList = new Gson().fromJson(s, OffersRestModel.Type);

            Realm realm = null;
            try { // I could use try-with-resources here
                realm = Realm.getDefaultInstance();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.insertOrUpdate(offersList);
                    }
                });
            } finally {
                if(realm != null) {
                    realm.close();
                }
            }
        } else {
            mRequest.deliverError(null);
        }

        /*if (s != null && !s.isEmpty()) {
            offersList = new Gson().fromJson(s, OffersRestModel.Type);
            mAdapter = new OffersRecyclerAdapter(offersList, mContext);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mRequest.deliverError(null);
        }*/
    }

    @Override
    public void onResume() {
        super.onResume();
        doRequest();
        Utils.sendGATracker(mActivity, "Offers");
    }

    @Override
    public void fragmentBecameVisible() {
        doRequest();
    }
}
