package su.livemechta.app.v2_news;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import su.livemechta.app.Conf;
import su.livemechta.app.R;
import su.livemechta.app.Utils;
import su.livemechta.app.common.RecyclerViewClickListener;
import su.livemechta.app.v2_rest.model.response.ExcursionsRestModel;

/**
 * Created by beta on 06.01.17.
 */

public class ExcursionsRecyclerAdapter extends RecyclerView.Adapter<ExcursionsRecyclerAdapter.ExcursionsViewHolder> {

    private List<ExcursionsRestModel> excursions;
    private Context mContext;

    public ExcursionsRecyclerAdapter(List<ExcursionsRestModel> excursions, Context context) {
        this.excursions = excursions;
        this.mContext = context;
    }

    /**
     * Создание новых View и ViewHolder элемента списка, которые впоследствии могут переиспользоваться.
     */
    @Override
    public ExcursionsRecyclerAdapter.ExcursionsViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_item_excursions, viewGroup, false);
        return new ExcursionsRecyclerAdapter.ExcursionsViewHolder(v, new RecyclerViewClickListener() {
            @Override
            public void onRowClicked(int position) {
                mContext.startActivity(Utils.openURL(excursions.get(i).getDetail_url()));
            }

            @Override
            public void onViewClicked(View v, int position) {
                //
            }
        });
    }

    /**
     * Заполнение виджетов View данными из элемента списка с номером i
     */
    @Override
    public void onBindViewHolder(ExcursionsRecyclerAdapter.ExcursionsViewHolder viewHolder, int i) {
        viewHolder.setItem(excursions.get(i));
    }

    @Override
    public int getItemCount() {
        return excursions.size();
    }

    public class ExcursionsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ExcursionsRestModel eItem;
        private RecyclerViewClickListener mListener;

        private TextView eTitle;
        private TextView eStartDate;
        private TextView eDesc;
        private ImageView eImage;

        public ExcursionsViewHolder(View itemView, RecyclerViewClickListener listener) {
            super(itemView);
            mListener = listener;
            itemView.setOnClickListener(this);

            eTitle = (TextView) itemView.findViewById(R.id.excursionsTitle);
            eDesc = (TextView) itemView.findViewById(R.id.excursionsDesc);
            eImage=  (ImageView)itemView.findViewById(R.id.excursionsImage);
        }

        public void setItem(ExcursionsRestModel item) {
            eItem = item;
            eTitle.setText(eItem.getTitle());
            eDesc.setText(eItem.getDescription());
            Picasso.with(mContext).load(Conf.URL_MAIN + item.getPhoto()).into(eImage);
        }

        @Override
        public void onClick(View view) {
            if (mListener != null)
                mListener.onRowClicked(getAdapterPosition());
        }
    }
}
