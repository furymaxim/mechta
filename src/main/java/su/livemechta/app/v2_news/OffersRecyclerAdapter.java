package su.livemechta.app.v2_news;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import su.livemechta.app.Conf;
import su.livemechta.app.R;
import su.livemechta.app.Utils;
import su.livemechta.app.common.RecyclerViewClickListener;
import su.livemechta.app.v2_rest.model.response.OffersRestModel;

/**
 * Created by beta on 06.01.17.
 */

public class OffersRecyclerAdapter extends RecyclerView.Adapter<OffersRecyclerAdapter.OffersViewHolder> {

    private List<OffersRestModel> offers;
    private Context mContext;

    public OffersRecyclerAdapter(List<OffersRestModel> offers, Context context) {
        this.offers = offers;
        this.mContext = context;
    }

    /**
     * Создание новых View и ViewHolder элемента списка, которые впоследствии могут переиспользоваться.
     */
    @Override
    public OffersRecyclerAdapter.OffersViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_item_offers, viewGroup, false);
        return new OffersRecyclerAdapter.OffersViewHolder(v, new RecyclerViewClickListener() {
            @Override
            public void onRowClicked(int position) {
                mContext.startActivity(Utils.openURL(offers.get(i).getDetail_url()));
            }

            @Override
            public void onViewClicked(View v, int position) {
                //
            }
        });
    }

    /**
     * Заполнение виджетов View данными из элемента списка с номером i
     */
    @Override
    public void onBindViewHolder(OffersRecyclerAdapter.OffersViewHolder viewHolder, int i) {
        viewHolder.setItem(offers.get(i));
    }

    @Override
    public int getItemCount() {
        return offers.size();
    }

    public class OffersViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private OffersRestModel oItem;
        private RecyclerViewClickListener mListener;

        private TextView oTitle;
        private TextView oDesc;
        private ImageView oImage;


        public OffersViewHolder(View itemView, RecyclerViewClickListener listener) {
            super(itemView);
            mListener = listener;
            itemView.setOnClickListener(this);

            oTitle = (TextView) itemView.findViewById(R.id.offersTitle);
            oDesc = (TextView) itemView.findViewById(R.id.offersDesc);
            oImage=  (ImageView)itemView.findViewById(R.id.offersImage);
        }

        public void setItem(OffersRestModel item) {
            oItem = item;
            oTitle.setText(oItem.getTitle());
            oDesc.setText(oItem.getDescription());
            Picasso.with(mContext).load(Conf.URL_MAIN + item.getPhoto()).into(oImage);
        }

        @Override
        public void onClick(View view) {
            if(mListener != null)
                mListener.onRowClicked(getAdapterPosition());
        }
    }
}
