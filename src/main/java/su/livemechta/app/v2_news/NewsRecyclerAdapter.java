package su.livemechta.app.v2_news;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import su.livemechta.app.Conf;
import su.livemechta.app.R;
import su.livemechta.app.Utils;
import su.livemechta.app.common.RecyclerViewClickListener;
import su.livemechta.app.v2_rest.model.response.NewsRestModel;

/**
 * Created by beta on 05.01.17.
 */

public class NewsRecyclerAdapter extends RecyclerView.Adapter<NewsRecyclerAdapter.NewsViewHolder> {

    private List<NewsRestModel> news;
    private Context mContext;

    public NewsRecyclerAdapter(List<NewsRestModel> news, Context context) {
        this.news = news;
        this.mContext = context;
    }

    /**
     * Создание новых View и ViewHolder элемента списка, которые впоследствии могут переиспользоваться.
     */
    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_item_news, viewGroup, false);
        return new NewsViewHolder(v, new RecyclerViewClickListener() {
            @Override
            public void onRowClicked(int position) {
                mContext.startActivity(Utils.openURL(news.get(i).getDetail_url()));
            }

            @Override
            public void onViewClicked(View v, int position) {
                //
            }
        });
    }

    /**
     * Заполнение виджетов View данными из элемента списка с номером i
     */
    @Override
    public void onBindViewHolder(NewsViewHolder viewHolder, int i) {
        viewHolder.setItem(news.get(i));
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private NewsRestModel nItem;
        private RecyclerViewClickListener mListener;

        private TextView nTitle;
        private TextView nDate;
        private TextView nDesc;
        private ImageView nImage;

        public NewsViewHolder(View itemView, RecyclerViewClickListener listener) {
            super(itemView);
            mListener = listener;
            itemView.setOnClickListener(this);

            nTitle = (TextView) itemView.findViewById(R.id.newsTitle);
            nDate = (TextView) itemView.findViewById(R.id.newsDate);
            nDesc = (TextView) itemView.findViewById(R.id.newsDesc);
            nImage = (ImageView) itemView.findViewById(R.id.newsImage);
        }

        public void setItem(NewsRestModel item) {
            nItem = item;
            nTitle.setText(nItem.getTitle());
            nDate.setText(nItem.getPublished_at().toString());
            nDesc.setText(nItem.getDescription());
            Picasso.with(mContext).load(Conf.URL_MAIN + nItem.getPhoto()).into(nImage);
        }

        @Override
        public void onClick(View view) {
            if(mListener != null)
                mListener.onRowClicked(getAdapterPosition());
        }
    }

}
