package su.livemechta.app.v2_callback;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dgreenhalgh.android.simpleitemdecoration.linear.DividerItemDecoration;

import su.livemechta.app.MainActivity;
import su.livemechta.app.R;
import su.livemechta.app.Utils;

/**
 * Created by beta on 08.01.17.
 */

public class CallbackFragment  extends Fragment implements View.OnClickListener {

    private Context mContext;
    private Activity mActivity;

    private Button callWhatsApp;
    private Button callViber;
    private Button callPhone;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        mActivity = getActivity();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) actionBar.setTitle(R.string.drawer_item_callback);

        View view = inflater.inflate(R.layout.fragment_callback, null);

        callWhatsApp = (Button) view.findViewById(R.id.buttonWhatsApp);
        callViber = (Button) view.findViewById(R.id.buttonViber);
        callPhone = (Button) view.findViewById(R.id.buttonPhone);

        //TODO номер с +7
        final String phone = "";

        callWhatsApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWhatsappContact(phone);
            }
        });

        callViber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openViberContact(phone);
            }
        });

        callPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPhoneContact(phone);
            }
        });

        if (appInstalledOrNot("com.whatsapp")) callWhatsApp.setVisibility(View.VISIBLE);
        if (appInstalledOrNot("com.viber.voip")) callViber.setVisibility(View.VISIBLE);

        return view;
    }

    void openWhatsappContact(String number) {
        Uri uri = Uri.parse("smsto:" + number);
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.setPackage("com.whatsapp");
        startActivity(Intent.createChooser(i, ""));
    }

    void openViberContact(String number) {
        Uri phone = Uri.parse("tel:"+number);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, phone);
        callIntent.setPackage("com.viber.voip");
        startActivity(callIntent);
    }

    void openPhoneContact(String number) {
        Uri phone = Uri.parse("tel:"+number);
        Intent callIntent = new Intent(Intent.ACTION_DIAL, phone);
        startActivity(callIntent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonWhatsApp:
                break;
            case R.id.buttonViber:
                break;
            case R.id.buttonPhone:
                break;
        }
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = mContext.getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.sendGATracker(mActivity, "Callback");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}