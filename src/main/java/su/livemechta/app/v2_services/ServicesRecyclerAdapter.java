package su.livemechta.app.v2_services;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import su.livemechta.app.R;
import su.livemechta.app.Utils;
import su.livemechta.app.common.RecyclerViewClickListener;
import su.livemechta.app.v2_rest.model.response.ServicesRestModel;

/**
 * Created by beta on 06.01.17.
 */

public class ServicesRecyclerAdapter extends RecyclerView.Adapter<ServicesRecyclerAdapter.ServicesViewHolder> {

    private List<ServicesRestModel> services;
    private Context mContext;

    public ServicesRecyclerAdapter(List<ServicesRestModel> services, Context context) {
        this.services = services;
        this.mContext = context;
    }

    /**
     * Создание новых View и ViewHolder элемента списка, которые впоследствии могут переиспользоваться.
     */
    @Override
    public ServicesRecyclerAdapter.ServicesViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_item_services, viewGroup, false);
        return new ServicesRecyclerAdapter.ServicesViewHolder(v, new RecyclerViewClickListener() {
            @Override
            public void onRowClicked(int position) {
                try {

                    Log.d("size",String.valueOf(services.size()));
                    Log.d("size",String.valueOf(services.get(i).getDetail_url()));
                    //mContext.startActivity(Utils.openURL(services.get(i).getDetail_url()));
                    mContext.startActivity(Utils.openURL("http://ya.ru"));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onViewClicked(View v, int position) {
                //
            }
        });
    }

    /**
     * Заполнение виджетов View данными из элемента списка с номером i
     */
    @Override
    public void onBindViewHolder(ServicesRecyclerAdapter.ServicesViewHolder viewHolder, int i) {
        viewHolder.setItem(services.get(i));

    }

    @Override
    public int getItemCount() {
        return services.size();
    }

    public class ServicesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ServicesRestModel sItem;
        private RecyclerViewClickListener mListener;

        private TextView sTitle;
        private TextView sDesc;

        public ServicesViewHolder(View itemView, RecyclerViewClickListener listener) {
            super(itemView);
            mListener = listener;
            itemView.setOnClickListener(this);

            sTitle = (TextView) itemView.findViewById(R.id.servicesTitle);
            sDesc = (TextView) itemView.findViewById(R.id.servicesDesc);
        }

        public void setItem(ServicesRestModel item) {
            sItem = item;
            sTitle.setText(sItem.getTitle());
            sDesc.setText(sItem.getDescription());
        }

        @Override
        public void onClick(View view) {
            if (mListener != null)
                mListener.onRowClicked(getAdapterPosition());
        }
    }
}
