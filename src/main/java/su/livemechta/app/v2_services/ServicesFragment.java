package su.livemechta.app.v2_services;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;

import su.livemechta.app.MainActivity;
import su.livemechta.app.R;
import su.livemechta.app.TabFragmentInterface;
import su.livemechta.app.Utils;

public class ServicesFragment extends Fragment {
    private String[] mTitles;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mTitles = getResources().getStringArray(R.array.services_tabs);

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) actionBar.setTitle(R.string.services);

        View view = inflater.inflate(R.layout.fragment_pager, null);

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Medium.ttf");
        tabs.setTypeface(tf, Typeface.BOLD);
        final ViewPager pager = (ViewPager) view.findViewById(R.id.pager);

        final PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());
        pager.setAdapter(adapter);
        tabs.setViewPager(pager);
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                TabFragmentInterface fragment = (TabFragmentInterface) adapter.instantiateItem(pager, i);
                if (fragment != null) {
                    fragment.fragmentBecameVisible();
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.sendGATracker(getActivity(), "Services");
    }

    public class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitles[position];
        }

        @Override
        public int getCount() {
            return mTitles.length;
        }

        @Override
        public Fragment getItem(int position) {
            Bundle b = new Bundle();

            switch (position){
                case 1:
                    b.putSerializable("service_type", ServiceType.svcInternal);
                    b.putString("type","internal");
                    break;
                case 2:
                    b.putSerializable("service_type", ServiceType.svcExternal);
                    b.putString("type","external");
                    break;
                default:
                    break;
            }

            Fragment fr = new ServicesTabRecyclerFragment();
            fr.setArguments(b);
            return fr;
        }
    }
}
