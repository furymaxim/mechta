package su.livemechta.app.v2_services;

/**
 * Created by beta on 09.01.17.
 */

enum ServiceType {
    svcInternal("internal"), svcExternal("external");

    private String stringValue;
    ServiceType(String toString) {
        stringValue = toString;
    }

    @Override
    public String toString() {
        return stringValue;
    }
}
