package su.livemechta.app.v2_settings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.dgreenhalgh.android.simpleitemdecoration.linear.DividerItemDecoration;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.List;

import su.livemechta.app.MainActivity;
import su.livemechta.app.R;
import su.livemechta.app.Settings;
import su.livemechta.app.Utils;

/**
 * Created by beta on 06.01.17.
 */

public class SettingsFragment extends Fragment {

    private Context mContext;
    private Activity mActivity;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private List<Settings> settingsList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        mActivity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) actionBar.setTitle(R.string.nav_settings);


        String[] mTitles = getResources().getStringArray(R.array.settings_elements);
        String[] mTopics = getResources().getStringArray(R.array.settings_topics);

        View view = inflater.inflate(R.layout.fragment_settings, null);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(mContext) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        mRecyclerView.setLayoutManager(mLayoutManager);

        Drawable dividerDrawable = ContextCompat.getDrawable(mContext, R.drawable.recycler_divider);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(dividerDrawable));

        settingsList = Utils.getSettings(getActivity());

        if(settingsList == null) {
            settingsList = new ArrayList<>();
            for (int i=0;i<mTopics.length;i++) {
                settingsList.add(new Settings(mTitles[i], mTopics[i], false));
            }
            Utils.setSettings(getActivity(), settingsList);
        }

        mAdapter = new SettingsRecyclerAdapter(settingsList);
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    public void setTopicChecked(int position, boolean subscribe) {
        if(subscribe) {
            Log.e("Subscribe", settingsList.get(position).getTopic());
            FirebaseMessaging.getInstance().subscribeToTopic(settingsList.get(position).getTopic());
        } else {
            Log.e("Unsubscribe", settingsList.get(position).getTopic());
            FirebaseMessaging.getInstance().unsubscribeFromTopic(settingsList.get(position).getTopic());
        }

        settingsList.get(position).setChecked(subscribe);

        Utils.setSettings(getActivity(), settingsList);
    }

    @Override
    public void onResume() {
        super.onResume();
        Utils.sendGATracker(getActivity(), "Settings");
    }

    public class SettingsRecyclerAdapter extends RecyclerView.Adapter<SettingsRecyclerAdapter.SettingsViewHolder> {

        private List<Settings> settings;

        public SettingsRecyclerAdapter(List<Settings> settings) {
            this.settings = settings;
        }

        @Override
        public SettingsRecyclerAdapter.SettingsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_item_settings, viewGroup, false);
            return new SettingsViewHolder(v);
        }

        @Override
        public void onBindViewHolder(SettingsRecyclerAdapter.SettingsViewHolder viewHolder, int i) {
            viewHolder.setItem(settings.get(i), i);
        }

        @Override
        public int getItemCount() {
            return settings.size();
        }

        public class SettingsViewHolder extends RecyclerView.ViewHolder {
            private TextView sTitle;
            private Switch sSwitch;
            private int position;

            public SettingsViewHolder(View itemView) {
                super(itemView);

                sTitle = (TextView) itemView.findViewById(R.id.settText);
                sSwitch = (Switch) itemView.findViewById(R.id.settSwitch);
                sSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        setTopicChecked(position, isChecked);
                    }
                });

            }

            public void setItem(Settings item, int position) {
                sTitle.setText(item.getName());
                sSwitch.setChecked(item.isChecked());
                this.position = position;
            }
        }
    }
}
