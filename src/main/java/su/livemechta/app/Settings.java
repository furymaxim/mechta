package su.livemechta.app;

public class Settings {

    private String name;

    private String topic;

    private boolean checked = false;

    public Settings() {
    }

    public Settings(String name, String topic, boolean checked) {
        this.name = name;
        this.topic = topic;
        this.checked = checked;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
