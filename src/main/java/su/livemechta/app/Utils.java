package su.livemechta.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Display;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class Utils {

    public static List<Settings> getSettings(Context context) {
        if(!PreferenceManager.getDefaultSharedPreferences(context).getString("Settings", "").equals("")) {
            return new Gson().fromJson(PreferenceManager.getDefaultSharedPreferences(context).getString("Settings", ""), new TypeToken<List<Settings>>() {}.getType());
        } else {
            return null;
        }
    }

    public static void setSettings(Context context, List<Settings> settingsList) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("Settings", new Gson().toJson(settingsList)).commit();
    }

    public static void sendGATracker(Activity a, String path) {
        Tracker t = ((MyApplication) a.getApplication()).getTracker();
        t.setScreenName(path);
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    public int getActionBarIcon(Activity c) {
        Display display = c.getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density = c.getResources().getDisplayMetrics().density;
        float dpWidth = outMetrics.widthPixels / density;

        return (dpWidth > 390) ? R.drawable.ic_logo : R.drawable.ic_logo_small;
    }

    public static Intent openURL(String url){
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        return new Intent(Intent.ACTION_VIEW, Uri.parse(url));
    }
}
