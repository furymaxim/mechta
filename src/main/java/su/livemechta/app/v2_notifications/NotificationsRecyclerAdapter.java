package su.livemechta.app.v2_notifications;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import su.livemechta.app.R;
import su.livemechta.app.Utils;
import su.livemechta.app.common.RecyclerViewClickListener;
import su.livemechta.app.v2_rest.model.response.NotificationsRestModel;
import su.livemechta.app.v2_rest.model.response.ServicesRestModel;
import su.livemechta.app.v2_services.ServicesRecyclerAdapter;

/**
 * Created by beta on 17.01.17.
 */

public class NotificationsRecyclerAdapter  extends RecyclerView.Adapter<NotificationsRecyclerAdapter.NotificationsViewHolder> {

    private List<NotificationsRestModel> notifications;
    private Context mContext;

    public NotificationsRecyclerAdapter(List<NotificationsRestModel> notifications, Context context) {
        this.notifications = notifications;
        this.mContext = context;
    }

    /**
     * Создание новых View и ViewHolder элемента списка, которые впоследствии могут переиспользоваться.
     */
    @Override
    public NotificationsRecyclerAdapter.NotificationsViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_item_notifications, viewGroup, false);
        return new NotificationsRecyclerAdapter.NotificationsViewHolder(v, new RecyclerViewClickListener() {
            @Override
            public void onRowClicked(int position) {
                //mContext.startActivity(Utils.openURL(services.get(i).getDetail_url()));
            }

            @Override
            public void onViewClicked(View v, int position) {
                //
            }
        });
    }

    /**
     * Заполнение виджетов View данными из элемента списка с номером i
     */
    @Override
    public void onBindViewHolder(NotificationsRecyclerAdapter.NotificationsViewHolder viewHolder, int i) {
        viewHolder.setItem(notifications.get(i));
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public class NotificationsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private NotificationsRestModel nItem;
        private RecyclerViewClickListener mListener;

        private TextView nText;
        private TextView nTime;
        private ImageView nImage;

        public NotificationsViewHolder(View itemView, RecyclerViewClickListener listener) {
            super(itemView);
            mListener = listener;
            itemView.setOnClickListener(this);

            nText = (TextView) itemView.findViewById(R.id.notifText);
            nTime = (TextView) itemView.findViewById(R.id.notifDate);
            nImage = (ImageView) itemView.findViewById(R.id.notifImage);
        }

        public void setItem(NotificationsRestModel item) {
            nItem = item;
            nText.setText(nItem.getBody());
            nTime.setText(nItem.getPublished_at().toString());
            nImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_decline));
        }

        @Override
        public void onClick(View view) {
            if (mListener != null)
                mListener.onRowClicked(getAdapterPosition());
        }
    }
}
