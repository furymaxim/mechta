package su.livemechta.app.v2_notifications;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dgreenhalgh.android.simpleitemdecoration.linear.DividerItemDecoration;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import rx.functions.Action1;
import su.livemechta.app.Conf;
import su.livemechta.app.MainActivity;
import su.livemechta.app.R;
import su.livemechta.app.Utils;
import su.livemechta.app.v2_rest.model.response.NotificationsRestModel;
import su.livemechta.app.volley.MyVolley;

/**
 * Created by beta on 17.01.17.
 */

public class NotificationsFragment extends Fragment implements Response.ErrorListener, Response.Listener<String> {

    private Context mContext;
    private Activity mActivity;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    private StringRequest mRequest;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        mActivity = getActivity();
        Realm.init(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) actionBar.setTitle(R.string.drawer_item_notifications);

        View view = inflater.inflate(R.layout.recyclerview, null);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Drawable dividerDrawable = ContextCompat.getDrawable(mContext, R.drawable.recycler_divider);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(dividerDrawable));

        return view;
    }

    private void doRequest() {
        Realm realm = Realm.getDefaultInstance();
        final RealmQuery<NotificationsRestModel> notifications = realm.where(NotificationsRestModel.class);

        notifications.findAll().sort("published_at", Sort.DESCENDING).asObservable()
                .subscribe(new Action1<RealmResults<NotificationsRestModel>>() {
                    @Override
                    public void call(RealmResults<NotificationsRestModel> notificationsRestModels) {
                        mAdapter = new NotificationsRecyclerAdapter(notificationsRestModels, mContext);
                        mRecyclerView.setAdapter(mAdapter);
                    }
                });

        mRequest = new StringRequest(Request.Method.GET,
                Conf.URL_NOTIFICATIONS,
                this, this);

        MyVolley.getRequestQueue().add(mRequest);
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        Toast.makeText(mContext, mContext.getString(R.string.need_conn), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponse(String s) {
        if (s != null && !s.isEmpty()) {
            Gson gson = new GsonBuilder().setDateFormat(NotificationsRestModel.DateFormat).create();
            final List<NotificationsRestModel> notificationsList = gson.fromJson(s, NotificationsRestModel.Type);

            Realm realm = null;
            try { // I could use try-with-resources here
                realm = Realm.getDefaultInstance();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.insertOrUpdate(notificationsList);
                    }
                });
            } finally {
                if(realm != null) {
                    realm.close();
                }
            }
        } else {
            mRequest.deliverError(null);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        doRequest();
        Utils.sendGATracker(mActivity, "Notifications");
    }
}
