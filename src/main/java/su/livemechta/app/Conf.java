package su.livemechta.app;

public class Conf {
    public static final String URL_MAIN = "https://dream.yii.website";
    public static final String URL_API = "/api/v1/";

    public static final String URL_NEWS = URL_MAIN + URL_API + "news";
    public static final String URL_EXCURSIONS = URL_MAIN + URL_API + "excursions";
    public static final String URL_SERVICES = URL_MAIN + URL_API + "services";
    public static final String URL_OFFERS = URL_MAIN + URL_API + "offers";
    public static final String URL_BUS_STOPS = URL_MAIN + URL_API + "bus_stops";
    public static final String URL_BUS_ROUTES = URL_MAIN + URL_API + "bus_routes";
    public static final String URL_BUS_CHANNELS = URL_MAIN + URL_API + "buses/channels?format=json";
    public static final String URL_NOTIFICATIONS = URL_MAIN + URL_API + "notifications";

    //public static final String URL_GCM_SERVER = "http://77.222.43.53/gcm-notifications/device/";
    public static final String URL_FCM_SERVER = "https://dream-push.herokuapp.com/gcm-notifications/device/";

    public static final String URL_SERVICES_INT = "internal";
    public static final String URL_SERVICES_EXT = "external";
}
